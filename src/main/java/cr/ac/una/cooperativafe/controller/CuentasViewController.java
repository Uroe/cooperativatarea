package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativafe.model.AsociadoDto;
import cr.ac.una.cooperativafe.model.CooperativaDto;
import cr.ac.una.cooperativafe.model.TipoCuentaDto;
import cr.ac.una.cooperativafe.service.AsociadoService;
import cr.ac.una.cooperativafe.service.TipoCuentaService;
import cr.ac.una.cooperativafe.util.AppContext;
import cr.ac.una.cooperativafe.util.Formato;
import cr.ac.una.cooperativafe.util.Mensaje;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

public class CuentasViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private TableView<TipoCuentaDto> tbvCuentas;
    @FXML
    private TableColumn<TipoCuentaDto, String> tbcCuentasAhorro;
    @FXML
    private TableView<AsociadoDto> tbvAsociados;
    @FXML
    private TableColumn<AsociadoDto, String> tbcNombre;
    @FXML
    private JFXTextField txtFolio;
    @FXML
    private JFXTextField txtNombre;

    CooperativaDto coope;
    AsociadoDto asociado;
    TipoCuentaDto tipoCuentaDto;
    List<TipoCuentaDto> listaTabla = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        txtFolio.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        asociado = new AsociadoDto();
        nuevoAsociado();

        coope = (CooperativaDto) AppContext.getInstance().get("cooperativa");
        TipoCuentaService service = new TipoCuentaService();
        Respuesta r = service.getTipoCuentas(coope.getId());
        if (r.getEstado()) {
            listaTabla = (List<TipoCuentaDto>) r.getResultado("TipoCuenta");
            tbvCuentas.getItems().addAll(listaTabla);
            tbvCuentas.refresh();
        }

        tbcCuentasAhorro.setText("Cuentas de " + coope.getNomCooperativa());
        tbcNombre.setText("Asociados");
        tbcCuentasAhorro.setCellValueFactory(cd -> cd.getValue().nomCuenta);
        tbcNombre.setCellValueFactory(cd -> cd.getValue().asoNombre);

        tbvCuentas.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends TipoCuentaDto> observable, TipoCuentaDto oldValue, TipoCuentaDto newValue)
                -> {
            if (newValue != null) {
                tipoCuentaDto = newValue;
                tbvAsociados.setItems(tipoCuentaDto.getAsociados());
            }
        });
    }

    private void bindAsociado(Boolean nuevo) {
        if (!nuevo) {
            txtFolio.textProperty().bind(asociado.asoFolio);
        }
        txtNombre.textProperty().bindBidirectional(asociado.asoNombre);
    }

    private void unbindAsociado() {
        txtFolio.textProperty().unbind();
        txtNombre.textProperty().unbindBidirectional(asociado.asoNombre);

    }

    private void nuevoAsociado() {
        unbindAsociado();
        asociado = new AsociadoDto();
        bindAsociado(true);
        txtFolio.clear();
        txtFolio.requestFocus();
    }

    @FXML
    private void AperturarCuenta(ActionEvent event) {

        if (asociado.getAsoFolio() == null || asociado.getAsoNombre().isEmpty()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Agregar Asociado", getStage(),
                    "Es necesario cargar un asociado para abrirle una cuenta.");
        } else if (tbvAsociados.getItems() == null || !tbvAsociados.getItems().stream().
                anyMatch(a -> a.asoFolio.get().equalsIgnoreCase(asociado.asoFolio.toString()))) {
            asociado.setModificado(true);
            tbvAsociados.getItems().add(asociado);
            tbvAsociados.refresh();
        }
        nuevoAsociado();

    }

    @FXML
    private void GuardarRegistro(ActionEvent event) {
        try {

            TipoCuentaService service = new TipoCuentaService();
            tipoCuentaDto.setCoop(coope);
            Respuesta respuesta = service.guardarTipoCuenta(tipoCuentaDto);
            if (!respuesta.getEstado()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Apertura de cuentas", getStage(), respuesta.getMensaje());
            } else {
                unbindAsociado();
                tipoCuentaDto = (TipoCuentaDto) respuesta.getResultado("TipoCuenta");
                bindAsociado(false);
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Apertura de cuentas", getStage(),
                        "Cuenta agregada correctamente");
            }

        } catch (Exception ex) {
            Logger.getLogger(CuentasViewController.class.getName()).log(Level.SEVERE, "Apertura de cuentas", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Cuenta", getStage(),
                    "Este asociado ya posee esta cuenta");
        }
    }

    @FXML
    private void onKeyPressedtxtFolio(KeyEvent event) {
        if (tipoCuentaDto != null) {
            if (event.getCode() == KeyCode.ENTER && !txtFolio.getText().isEmpty()) {
                cargarAsociado(Long.valueOf(txtFolio.getText()));

            }
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cuentas", getStage(),
                    "Primero selecciona la cuenta que desea aperturar");
        }
    }

    private void cargarAsociado(Long folio) {

        AsociadoService service = new AsociadoService();
        Respuesta respuesta = service.getAsociado(folio);

        if (respuesta.getEstado()) {
            unbindAsociado();
            asociado = (AsociadoDto) respuesta.getResultado("Asociado");
            bindAsociado(false);
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Asociado",
                    getStage(), respuesta.getMensaje());
        }
    }

    @Override
    public void initialize() {
    }

    @FXML
    private void LimpiarTabla(ActionEvent event) {
        tbvAsociados.getItems().clear();
    }

}
