package cr.ac.una.cooperativafe.model;

import cr.ac.una.cooperativafe.model.TipoCuenta;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2020-06-02T13:36:39", comments="EclipseLink-2.7.4.v20190115-rNA")
@StaticMetamodel(Asociado.class)
public class Asociado_ { 

    public static volatile SingularAttribute<Asociado, Integer> grado;
    public static volatile ListAttribute<Asociado, TipoCuenta> tipocuentas;
    public static volatile SingularAttribute<Asociado, String> sApellido;
    public static volatile SingularAttribute<Asociado, byte[]> foto;
    public static volatile SingularAttribute<Asociado, String> genero;
    public static volatile SingularAttribute<Asociado, Long> folio;
    public static volatile SingularAttribute<Asociado, String> pApellido;
    public static volatile SingularAttribute<Asociado, String> nombre;
    public static volatile SingularAttribute<Asociado, Integer> edad;
    public static volatile SingularAttribute<Asociado, Long> version;

}