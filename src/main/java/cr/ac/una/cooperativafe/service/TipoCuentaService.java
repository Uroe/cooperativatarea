/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.cooperativafe.service;

import cr.ac.una.cooperativafe.model.Asociado;
import cr.ac.una.cooperativafe.model.AsociadoDto;
import cr.ac.una.cooperativafe.model.Cooperativa;
import cr.ac.una.cooperativafe.model.TipoCuenta;
import cr.ac.una.cooperativafe.model.TipoCuentaDto;
import cr.ac.una.cooperativafe.util.EntityManagerHelper;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import oracle.net.aso.a;

/**
 *
 * @author Navarro
 */
public class TipoCuentaService {

    EntityManager em = EntityManagerHelper.getInstance().getManager();
    private EntityTransaction et;

    public Respuesta getTipoCuenta(Long id) {
        try {
            Query qryTipoCuenta = em.createNamedQuery("TipoCuenta.findByTcuenId", TipoCuenta.class);
            qryTipoCuenta.setParameter("id", id);
           
            TipoCuenta tipoCuenta = (TipoCuenta) qryTipoCuenta.getSingleResult();
            TipoCuentaDto tipoCuentaDto = new TipoCuentaDto(tipoCuenta);
            for (Asociado aso : tipoCuenta.getAsociados()) {
                tipoCuentaDto.getAsociados().add(new AsociadoDto(aso));
            }

            return new Respuesta(true, "", "", "TipoCuenta", tipoCuentaDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, "No existe un tipo de cuenta con la identificacion ingresada.", "getTipoCuentaNoResultException");
        } catch (NonUniqueResultException ex) {
            Logger.getLogger(CooperativaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar el tipo de cuenta.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar el tipo de cuenta.", "getTipoCuenta NonUniqueResultException");
        } catch (Exception ex) {
            Logger.getLogger(TipoCuentaService.class.getName()).log(Level.SEVERE, "Ocurrio un error obteniendo el tipo de cuenta [" + id + "]", ex);
            return new Respuesta(false, "Ocurrio un error obteniendo el tipo de cuenta.", "getTipoCuenta" + ex.getMessage());
        }
    }

    public Respuesta getTipoCuentas(Long id) {
        try {
            Query qryTipoCuenta = em.createNamedQuery("TipoCuenta.findAll", TipoCuenta.class);

            List<TipoCuenta> tCuenta = (List<TipoCuenta>) qryTipoCuenta.getResultList();
            List<TipoCuenta> tCuentas = new ArrayList<TipoCuenta>();
            Cooperativa coop;
            for (TipoCuenta tipoCuenta : tCuenta) {
                coop = tipoCuenta.getTcuenIdcoop();
                if (coop.getId() == id) {
                    tCuentas.add(tipoCuenta);
                }
            }
            List<TipoCuentaDto> tCuentaDto = new ArrayList<TipoCuentaDto>();
            for (TipoCuenta tipoCuenta : tCuentas) {
                tCuentaDto.add(new TipoCuentaDto(tipoCuenta));
            }
            return new Respuesta(true, "", "", "TipoCuenta", tCuentaDto);

        } catch (NonUniqueResultException ex) {
            Logger.getLogger(CooperativaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar el tipo de cuenta.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar el tipo de cuenta.", "getTipoCuentas NonUniqueResultException");
        }
    }

    public Respuesta guardarTipoCuenta(TipoCuentaDto tcuentaDto) {
        try {
            et = em.getTransaction();
            et.begin();
            Cooperativa coop = em.find(Cooperativa.class, tcuentaDto.getCoop().getId());
            if (coop == null) {
                et.rollback();
                return new Respuesta(false, "No se encontró una cooperativa relacionada a tipo de cuenta", "guardarTipoCuenta NoResultException");
            } else {
                TipoCuenta tCuenta;
                if (tcuentaDto.getId() != null && tcuentaDto.getId() > 0) {
                    tCuenta = em.find(TipoCuenta.class, tcuentaDto.getId());
                    if (tCuenta == null) {
                        et.rollback();
                        return new Respuesta(false, "No se encontro el tipo de cuenta a modificar", "guardarTipoCuenta NoResultException");
                    }
                    tCuenta.actualizarTipoCuenta(tcuentaDto);
                    tCuenta.setTcuenIdcoop(coop);
//                    tCuenta = em.merge(tCuenta);
                    if (!tcuentaDto.getAsociados().isEmpty()) {
                        for (AsociadoDto aso : tcuentaDto.getAsociados()) {
                            if (aso.getModificado()) {
                                Asociado asociado = em.find(Asociado.class, aso.getAsoFolio());
                                asociado.getTipocuentas().add(tCuenta);
                                tCuenta.getAsociados().add(asociado);
                            }
                        }
                    }
                    tCuenta = em.merge(tCuenta);
                } else {

                    tCuenta = new TipoCuenta(tcuentaDto);
                    tCuenta.setTcuenIdcoop(coop);
                    em.persist(tCuenta);
                }
                et.commit();
                return new Respuesta(true, "", "", "TipoCuenta", new TipoCuentaDto(tCuenta));
            }
        } catch (IllegalStateException ex) {
            return new Respuesta(false, "Esa cuenta ya fue añadida a este asociado.", "guardarTipoCuenta " + ex.getMessage());

        } catch (Exception ex) {
            et.rollback();
            Logger.getLogger(CooperativaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardando el tipo de cuenta.", ex);
            return new Respuesta(false, "Ocurrio un error al guardando el tipo de cuenta.", "guardarTipoCuenta " + ex.getMessage());
        }

    }
    
    public Respuesta eliminarTipoCuenta(Long id) {
        try {
            et = em.getTransaction();
            et.begin();
            TipoCuenta tipoCuenta;
            if (id != null && id > 0) {
                tipoCuenta = em.find(TipoCuenta.class,
                        id);
                if (tipoCuenta == null) {
                    et.rollback();
                    return new Respuesta(false, "No se encontró el tipo de cuenta a eliminar", "eliminarTipoCuenta NoResultException");
                }
                em.remove(tipoCuenta);
            } else {
                et.rollback();
                return new Respuesta(false, "Debe cargar el tipo de cuenta a eliminar", "eliminarTipoCuenta NoResultException");
            }
            et.commit();
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            et.rollback();
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(
                        false, "No se puede eliminar el tipo de cuenta porque tiene relaciones con otros registros", "eliminarTipoCuenta" + ex.getMessage());
            }
            Logger.getLogger(CooperativaService.class
                    .getName()).log(Level.SEVERE, "Ocurio un error al eliminar el tipo de cuenta.", ex);
            return new Respuesta(false, "Ocuarrio un error al eliminar el tipo de cuenta.", "eliminarTipoCuenta" + ex.getMessage());
        }
    }
}
