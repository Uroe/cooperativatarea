package cr.ac.una.cooperativafe.controller;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.DeviceGray;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.BorderRadius;
import com.itextpdf.layout.property.TextAlignment;
import com.jfoenix.controls.JFXButton;
import cr.ac.una.cooperativafe.model.AsociadoDto;
import cr.ac.una.cooperativafe.model.CooperativaDto;
import cr.ac.una.cooperativafe.util.AppContext;
import cr.ac.una.cooperativafe.util.Mensaje;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class CarnetViewController extends Controller implements Initializable {
    
    @FXML
    private AnchorPane root;
    @FXML
    private Label lbelNomCoop;
    @FXML
    private Label lbelFolio;
    @FXML
    private Label lbelNombre;
    @FXML
    private Label lbelApellido;
    @FXML
    private ImageView imaView;
    @FXML
    private JFXButton btnCrearCarnet;
    
    AsociadoDto asociado;
    CooperativaDto coop;
    File fichero;
    String rutaFichero;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        asociado = (AsociadoDto) AppContext.getInstance().get("asociado");
        //Nombre del archivo guardado 
        fichero = new File("carnet.pdf");
        //Ruta donde se guarda el carnet 
        rutaFichero = fichero.getAbsolutePath();
        
        coop = (CooperativaDto) AppContext.getInstance().get("cooperativa"); 
        lbelNomCoop.setText(coop.getNomCooperativa());
        lbelFolio.setText(asociado.getAsoFolio().toString());
        lbelNombre.setText(asociado.getAsoNombre());
        lbelApellido.setText(asociado.getAsoPapellido());
        ByteArrayInputStream bis = new ByteArrayInputStream(asociado.asoFoto);//Para cambiar el formato de  la imagen
        imaView.setImage(new javafx.scene.image.Image(bis));
    }
    
    @FXML
    private void onActionBtnCrearCarnet(ActionEvent event) throws FileNotFoundException, MalformedURLException, IOException {
        PdfWriter writer = new PdfWriter(rutaFichero); //Se encarga de escribir el pdf
        PdfDocument pdfDoc = new PdfDocument(writer); // Se encarga de administra lo que se escribe en el pdf
       
        try (Document doc = new Document(pdfDoc, PageSize.A6.rotate())) { /*Se encarga del diseño del pdf como el tipo de hoja y margenes*/
            doc.setMargins(0, 0, 0, 0);
            Image molde = new Image(ImageDataFactory.create("src/main/resources/cr/ac/una/"
                    + "cooperativafe/resources/CarnetMolde.png"));//Imagen del diseño del carnet de asociado
            Image fotoAsociado = new Image(ImageDataFactory.create(asociado.getAsoFoto()));//Foto del asociado
            //Tamaño de la foto del asociado
            fotoAsociado.setHeight(150);
            fotoAsociado.setWidth(130);
            fotoAsociado.setBorderRadius(new BorderRadius(20));
            molde.scaleToFit(420, 300);
            doc.add(getInfo(pdfDoc, molde, fotoAsociado));
            doc.close();//Es para indicar el final del pdf
        }
        new Mensaje().showModal(Alert.AlertType.INFORMATION, "Crear Carnet", getStage(),
                "Carnet creado correctamente");
    }
    
    public Image getInfo(PdfDocument pdfDoc, Image molde, Image foto) throws IOException {
        
        float width = molde.getImageScaledWidth();
        float height = molde.getImageScaledHeight();
        ////////////////////// 
        Paragraph p = new Paragraph();//Se encarga de crear un parrafo en el pfd
        p.add(foto);
        ////////////////////// 
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);//Da el estilo de las letra
        PdfFormXObject template = new PdfFormXObject(new Rectangle(width, height));
        ////////////////////// 
        //Se encarga de escribir un texto con ayuda de la coodenadas "x" y "y"
        new Canvas(template, pdfDoc).add(molde).
                setFontColor(DeviceGray.BLACK).setFontSize(25).setFont(font).
                showTextAligned(coop.getNomCooperativa(), 225, 248, TextAlignment.JUSTIFIED).
                setFontColor(DeviceGray.BLACK).setFontSize(16).setFont(font).
                showTextAligned(asociado.getAsoNombre(), 295, 146, TextAlignment.JUSTIFIED).
                showTextAligned(asociado.getAsoPapellido(), 295, 112, TextAlignment.JUSTIFIED).
                showTextAligned(asociado.getAsoFolio().toString(), 295, 179, TextAlignment.JUSTIFIED).
                showTextAligned(p, 102, 80, TextAlignment.CENTER);
        //////////////////////    
        return new Image(template);
    }
    
    @Override
    public void initialize() {
        
    }
}
