/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.cooperativafe.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Navarro
 */
@Entity
@Table(name = "COOPFE_TIPOCUENTAS", schema = "una")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoCuenta.findAll", query = "SELECT t FROM TipoCuenta t"),
    @NamedQuery(name = "TipoCuenta.findByTcuenId", query = "SELECT t FROM TipoCuenta t WHERE t.id = :id")})
public class TipoCuenta implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "COOPFE_TIPOCUENTAS_TCUEN_ID_GENERATOR", sequenceName = "una.COOPFE_TIPOCUENTAS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COOPFE_TIPOCUENTAS_TCUEN_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "TCUEN_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "TCUEN_NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "TCUEN_PLAZOT")
    private String plazoTiempo;
    @Column(name = "TCUEN_ULTRETIRO")
    private Integer ultimoRetiro;
    @Column(name = "TCUEN_ULTDEPOSITO")
    private Integer ultimoDeposito;
    @Column(name = "TCUEN_SALDO")
    private Integer saldo;
    @Column(name = "TCUEN_FRETIRO")
    private LocalDate fechaRetiro;
    @Column(name = "TCUEN_FDEPOSITO")
    private LocalDate fechaDeposito;
    @Basic(optional = false)
    @Column(name = "TCUEN_ESTADO")
    private String estado;
    @Version
    @Basic(optional = false)
    @Column(name = "TCUEN_VERSION")
    private Long version;
    @JoinTable(name = "COOPFE_ASOCIADOSCUENTA", joinColumns = {
        @JoinColumn(name = "AXC_IDTCUEN", referencedColumnName = "TCUEN_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "AXC_FOLIOASO", referencedColumnName = "ASO_FOLIO")})
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Asociado> asociados;
    @JoinColumn(name = "TCUEN_IDCOOP", referencedColumnName = "COOP_ID")
    @OneToOne(optional = false)
    private Cooperativa tcuenIdcoop;

    public TipoCuenta() {
    }

    public TipoCuenta(Long id) {
        this.id = id;
    }

    public TipoCuenta(Long id, String nombre, String plazoTiempo, String estado, Long version) {
        this.id = id;
        this.nombre = nombre;
        this.plazoTiempo = plazoTiempo;
        this.estado = estado;
//        this.version = version;
    }

    public TipoCuenta(TipoCuentaDto tipoCuenta) {
        this.id = tipoCuenta.getId();
        actualizarTipoCuenta(tipoCuenta);
    }

    public void actualizarTipoCuenta(TipoCuentaDto tipoCuenta) {
        this.nombre = tipoCuenta.getNomCuenta();
        this.plazoTiempo = tipoCuenta.getPlazoTiempo();
        this.ultimoRetiro = 0; //Integer.valueOf(tipoCuenta.getUltimoRetiro());
        this.ultimoDeposito = 0; ///Integer.valueOf(tipoCuenta.getUltimoDeposito());
        this.saldo = 0; // Integer.valueOf(tipoCuenta.getSaldo());
        this.fechaRetiro = tipoCuenta.getFechaRetiro();
        this.fechaDeposito = tipoCuenta.getFechaDeposito();
        this.estado = tipoCuenta.getEstado();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPlazoTiempo() {
        return plazoTiempo;
    }

    public void setPlazoTiempo(String plazoTiempo) {
        this.plazoTiempo = plazoTiempo;
    }

    public Integer getUltimoRetiro() {
        return ultimoRetiro;
    }

    public void setUltimoRetiro(Integer ultimoRetiro) {
        this.ultimoRetiro = ultimoRetiro;
    }

    public Integer getUltimoDeposito() {
        return ultimoDeposito;
    }

    public void setUltimoDeposito(Integer ultimoDeposito) {
        this.ultimoDeposito = ultimoDeposito;
    }

    public Integer getSaldo() {
        return saldo;
    }

    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }

    public LocalDate getFechaRetiro() {
        return fechaDeposito;
    }

    public void setFechaRetiro(LocalDate fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    public LocalDate getFechaDeposito() {
        return fechaDeposito;
    }

    public void setFechaDeposito(LocalDate fechaDeposito) {
        this.fechaDeposito = fechaDeposito;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @XmlTransient
    public List<Asociado> getAsociados() {
        return asociados;
    }

    public void setAsociados(List<Asociado> asociados) {
        this.asociados = asociados;
    }

    public Cooperativa getTcuenIdcoop() {
        return tcuenIdcoop;
    }

    public void setTcuenIdcoop(Cooperativa tcuenIdcoop) {
        this.tcuenIdcoop = tcuenIdcoop;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoCuenta)) {
            return false;
        }
        TipoCuenta other = (TipoCuenta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.cooperativafe.model.TipoCuenta[ tcuenId=" + id + " ]";
    }

}
