/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativafe.model.AsociadoDto;
import cr.ac.una.cooperativafe.model.TipoCuentaDto;
import cr.ac.una.cooperativafe.service.AsociadoService;
import cr.ac.una.cooperativafe.util.AppContext;
import cr.ac.una.cooperativafe.util.Formato;
import cr.ac.una.cooperativafe.util.Mensaje;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class DepositosViewController extends Controller implements Initializable {

    @FXML
    private JFXButton btnUno;
    @FXML
    private JFXButton btnDos;
    @FXML
    private JFXButton btnTres;
    @FXML
    private JFXButton btnCuatro;
    @FXML
    private JFXButton btnCinco;
    @FXML
    private JFXButton btnSeis;
    @FXML
    private JFXButton btnSiete;
    @FXML
    private JFXButton btnOcho;
    @FXML
    private JFXButton btnNueve;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnCero;
    @FXML
    private JFXButton btnBorrar;
    @FXML
    private JFXTextField txtFolio;
    @FXML
    private Label lbelAsoNombre;
    @FXML
    private Label lbelAsoApellido;
    @FXML
    private JFXTextField txtMonto;
    @FXML
    private JFXButton btnDeposito;
    @FXML
    private TabPane tbpTransaccion;
    @FXML
    private Tab tbpCuenta;
    @FXML
    private Tab tbpDeposito;
    @FXML
    private TableView<TipoCuentaDto> tbvListaCuentas;
    @FXML
    private TableColumn<TipoCuentaDto, String> tbcCuenta;
    @FXML
    private TableColumn<TipoCuentaDto, Boolean> tbcSeleccionar;

    private AsociadoDto asociado;
    private TipoCuentaDto tCuenta;
    private final int limite = 5;

    ObservableList<TipoCuentaDto> transacion;

    Date fecha = new Date();
    LocalDate cFecha = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtFolio.setTextFormatter(Formato.getInstance().integerFormat());
        asociado = new AsociadoDto();
        nuevoAsociado();

        txtMonto.setTextFormatter(Formato.getInstance().cedulaFormat(5));
        tCuenta = new TipoCuentaDto();

        tbcCuenta.setCellValueFactory(cd -> cd.getValue().nomCuenta);

        tbcSeleccionar.setCellValueFactory((TableColumn.CellDataFeatures< TipoCuentaDto, Boolean> p)
                -> new SimpleBooleanProperty(p.getValue() != null));

        tbcSeleccionar.setCellFactory((TableColumn<TipoCuentaDto, Boolean> p) -> new DepositosViewController.ButtonCell());
    }
    
    private void bindAsociado(Boolean nuevo) {
        if (!nuevo) {
            txtFolio.textProperty().bind(asociado.asoFolio);
        }
    }

    private void unbindAsociado() {
        txtFolio.textProperty().unbind();
    }

    private void bindTipoCuenta(Boolean nuevo) {
        txtMonto.textProperty().bindBidirectional(tCuenta.ultimoRetiro);
    }

    private void unbindTipoCuenta() {
        txtMonto.textProperty().unbindBidirectional(tCuenta.ultimoRetiro);
    }
    
    private void nuevoAsociado() {
        unbindAsociado();
        asociado = new AsociadoDto();
        bindAsociado(true);
        txtFolio.clear();
        txtFolio.requestFocus();
    }

    private void nuevoRetiro() {
        txtMonto.clear();
        txtMonto.requestFocus();
    }

    private void cargarAsociado(Long folio) {
        AsociadoService service = new AsociadoService();
        Respuesta respuesta = service.getAsociado(folio);

        if (respuesta.getEstado()) {
            unbindAsociado();
            asociado = (AsociadoDto) respuesta.getResultado("Asociado");
            bindAsociado(false);
            lbelAsoNombre.setText(asociado.getAsoNombre());
            lbelAsoApellido.setText(asociado.getAsoPapellido());
            cargarTCuentas();

        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar asociado", getStage(), respuesta.getMensaje());
        }
    }

    private void cargarTCuentas() {
        tbvListaCuentas.getItems().clear();
        tbvListaCuentas.setItems(asociado.getCuentas());
        tbvListaCuentas.refresh();
    }
    
     @FXML
    private void onKeyPressedtxtFolio(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtFolio.getText().isEmpty()) {
            cargarAsociado(Long.valueOf(txtFolio.getText()));
        }
    }

    @FXML
    private void onSelectionChangedTabDeposito(Event event) {
         if (tbpDeposito.isSelected()) {
            if (asociado.getAsoFolio() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Transacion", getStage(), "Dede cargar el tipo de cuenta al que desea realizar un transaccion");
                tbpTransaccion.getSelectionModel().select(tbpCuenta);
            }
        } else if (tbpCuenta.isSelected()) {

        }
    }

    @FXML
    private void onActionValor(ActionEvent event) {
        if (txtMonto.getText().length() <= limite) {
            txtMonto.setText(txtMonto.getText() + ((Button) event.getSource()).getText());
        } else {
            if (!(txtMonto.getText().length() == 0)) {
                txtMonto.setText(txtMonto.getText().substring(0, txtMonto.getText().length() - 1));
            }
        }
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        txtMonto.setText("");
    }

    @FXML
    private void onActionBtnBorrar(ActionEvent event) {
        if (!(txtMonto.getText().length() == 0)) {
            txtMonto.setText(txtMonto.getText().substring(0, txtMonto.getText().length() - 1));
        }
    }

    @FXML
    private void onActionBtnDeposito(ActionEvent event) {
    }

    private class ButtonCell extends TableCell<TipoCuentaDto, Boolean> {

        final Button cellButton = new Button();

        ButtonCell() {
            cellButton.setPrefWidth(500);
            cellButton.getStyleClass().add("jfx-btnimg-tbvseleccionar");
            cellButton.setOnAction((event) -> {
                TipoCuentaDto tipoCuenta = ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                AppContext.getInstance().set("tipoCuenta", tipoCuenta);
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar Cuenta", getStage(),
                        "Cuenta Seleccionada");

            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty); //To change body of generated methods, choose Tools | Templates.
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }
    
    @Override
    public void initialize() {

    }
}
