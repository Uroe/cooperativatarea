package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.cooperativafe.model.AsociadoDto;
import cr.ac.una.cooperativafe.model.TipoCuentaDto;
import cr.ac.una.cooperativafe.service.AsociadoService;
import cr.ac.una.cooperativafe.util.FlowController;
import cr.ac.una.cooperativafe.util.Formato;
import cr.ac.una.cooperativafe.util.Mensaje;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class AsociadosViewController extends Controller implements Initializable {

    @FXML
    private TextField txtIngresarFolio;
    @FXML
    private JFXButton btnSaldo;
    @FXML
    private JFXButton btnCerrarSesion;
    @FXML
    private Label labelNombreAsociado;
    @FXML
    private TableView<TipoCuentaDto> tableEstadoCuenta;
    @FXML
    private TableColumn<TipoCuentaDto, String> ColumnSaldo;
    @FXML
    private TableColumn<TipoCuentaDto, String> columnNomCuenta;
    @FXML
    private TableColumn<TipoCuentaDto, String> columnPlazo;
    @FXML
    private BorderPane paneConsultas;
    @FXML
    private AnchorPane paneLogin;

    AsociadoDto asociado;
    ObservableList<TipoCuentaDto> cuentas;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtIngresarFolio.setTextFormatter(Formato.getInstance().integerFormat());
        asociado = new AsociadoDto();
        paneLogin.setVisible(true);
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void onActiontxtIngresarFolio(ActionEvent event) {
    }

    @FXML
    private void onActionbtnIniciarSesion(ActionEvent event) {

        if (txtIngresarFolio.getText().isEmpty()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar asociado", getStage(),
                    "Debe ingresar su folio");
        }
        AsociadoService service = new AsociadoService();
        Respuesta respuesta = service.getAsociado(Long.valueOf(txtIngresarFolio.getText()));

        if (respuesta.getEstado()) {
            asociado = (AsociadoDto) respuesta.getResultado("Asociado");
            paneLogin.setVisible(false);
            paneConsultas.setVisible(true);
            labelNombreAsociado.setText(asociado.getAsoNombre() + " " + asociado.getAsoPapellido());
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar asociado", getStage(),
                    "Ese folio no existe");
        }

    }

    @FXML
    private void onActionbtnCrearCuenta(ActionEvent event) {
        FlowController.getInstance().goView("RegistroView");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionbtnColsultarSaldo(ActionEvent event) {

        columnNomCuenta.setCellValueFactory(cd -> cd.getValue().nomCuenta);
        ColumnSaldo.setCellValueFactory(cd -> cd.getValue().saldo);
        columnPlazo.setCellValueFactory(cd -> cd.getValue().plazoTiempo);
        tableEstadoCuenta.setItems(asociado.getCuentas());
    }

    @FXML
    private void onActionbtnCerrarSesion(ActionEvent event) {
        paneConsultas.setVisible(false);
        paneLogin.setVisible(true);
        txtIngresarFolio.clear();
    }

}
