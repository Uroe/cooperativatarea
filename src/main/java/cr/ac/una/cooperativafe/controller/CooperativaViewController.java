package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativafe.model.CooperativaDto;
import cr.ac.una.cooperativafe.service.CooperativaService;
import cr.ac.una.cooperativafe.util.AppContext;
import cr.ac.una.cooperativafe.util.Formato;
import cr.ac.una.cooperativafe.util.Mensaje;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class CooperativaViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXTextField txtCooperativa;
    @FXML
    private JFXTextField txtEscuela;
    @FXML
    private JFXTextField txtProfesor;
    @FXML
    private JFXTextField txtAnolectivo;
    @FXML
    private TableView<CooperativaDto> tbvCooperativa;
    @FXML
    private TableColumn<CooperativaDto, String> tbcId;
    @FXML
    private TableColumn<CooperativaDto, String> tbcNombreCoop;
    @FXML
    private TableColumn<CooperativaDto, String> tbcAnoLectivo;
    @FXML
    private TableColumn<CooperativaDto, Boolean> tbcSeleccionar;
    @FXML
    private JFXButton btnNuevo;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXCheckBox chkActivo;

    CooperativaDto coopDto;
    List<Node> requeridos = new ArrayList<>();
    CooperativaService service = new CooperativaService();
    List<CooperativaDto> listaTabla = new ArrayList<>();
    
    /**
     * Initializes the controller class.
     */

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtCooperativa.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtEscuela.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtProfesor.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtAnolectivo.setTextFormatter(Formato.getInstance().cedulaFormat(5));
        coopDto = new CooperativaDto();
        nuevoCooperativa();
        indicarRequeridos();

        Respuesta r = service.getCooperativas();

        listaTabla =(List<CooperativaDto>) r.getResultado("Cooperativa");
        tbvCooperativa.getItems().addAll(listaTabla);
        tbvCooperativa.refresh();

        tbcId.setCellValueFactory(cd -> cd.getValue().id);
        tbcNombreCoop.setCellValueFactory(cd -> cd.getValue().nomCooperativa);
        tbcAnoLectivo.setCellValueFactory(cd -> cd.getValue().anoLectivo);

        tbvCooperativa.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends CooperativaDto> observable, CooperativaDto oldValue, CooperativaDto newValue) -> {
            unbindCooperativa();
            if (newValue != null) {
                coopDto = newValue;
                bindCooperativa(false);
            }
        });

        tbcSeleccionar.setCellValueFactory((TableColumn.CellDataFeatures< CooperativaDto, Boolean> p)
                -> new SimpleBooleanProperty(p.getValue() != null));

        tbcSeleccionar.setCellFactory((TableColumn<CooperativaDto, Boolean> p) -> new ButtonCell());
    }

    private void bindCooperativa(Boolean nuevo) {
        if (!nuevo) {
            txtId.textProperty().bind(coopDto.id);
        }
        txtCooperativa.textProperty().bindBidirectional(coopDto.nomCooperativa);
        txtEscuela.textProperty().bindBidirectional(coopDto.nomEscuela);
        txtProfesor.textProperty().bindBidirectional(coopDto.nomProfesor);
        txtAnolectivo.textProperty().bindBidirectional(coopDto.anoLectivo);
        chkActivo.selectedProperty().bindBidirectional(coopDto.estado);
    }

    private void unbindCooperativa() {
        txtId.textProperty().unbind();
        txtCooperativa.textProperty().unbindBidirectional(coopDto.nomCooperativa);
        txtEscuela.textProperty().unbindBidirectional(coopDto.nomEscuela);
        txtProfesor.textProperty().unbindBidirectional(coopDto.nomProfesor);
        txtAnolectivo.textProperty().unbindBidirectional(coopDto.anoLectivo);
        chkActivo.selectedProperty().unbindBidirectional(coopDto.estado);
    }

    private void nuevoCooperativa() {
        unbindCooperativa();
        tbvCooperativa.getSelectionModel().select(null);
        coopDto = new CooperativaDto();
        bindCooperativa(true);
        txtId.clear();
        txtId.requestFocus();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtCooperativa, txtEscuela, txtAnolectivo));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = " ";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && ((JFXTextField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && ((JFXPasswordField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato (" + invalidos + ").";
        }
    }

    @FXML
    private void onKeyPressedTxtId(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtId.getText().isEmpty()) {
            cargarCooperativa(Long.valueOf(txtId.getText()));
        }
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Cooperativa", getStage(), "Esta seguro que desea limpiar el registro?")) {
            nuevoCooperativa();
        }
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        try {
            if (coopDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar cooperativa", getStage(), "Dede cargar la cooperativa que desea eliminar");
            } else {
                CooperativaService service = new CooperativaService();
                Respuesta respuesta = service.eliminarCooperativa(coopDto.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar cooperativa", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar cooperativa", getStage(), "Planilla eliminado correctamente");
                    listaTabla.remove(coopDto);
                    tbvCooperativa.getItems().remove(coopDto);
                    tbvCooperativa.refresh();
                    AppContext.getInstance().delete("cooperativa");
                    //nuevoCooperativa();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CooperativaViewController.class.getName()).log(Level.SEVERE, "Error eliminando la cooperativa.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar cooperativa", getStage(), "Ocurrio un error eliminando la cooperativa");
        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar cooperativa", getStage(), invalidos);
            } else {
                CooperativaService service = new CooperativaService();
                Respuesta respuesta = service.guardarCooperativa(coopDto);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar cooperativa", getStage(), respuesta.getMensaje());
                } else {
                    unbindCooperativa();
                    coopDto = (CooperativaDto) respuesta.getResultado("Cooperativa");
                    bindCooperativa(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar cooperativa", getStage(), "Cooperativa guardado correctamente");
                    actualizarTabla();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CooperativaViewController.class.getName()).log(Level.SEVERE, "Error guardando la cooperativa.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar cooperativa", getStage(), "Ocurrio un error guardando la cooperativa");
        }
    }

    private void actualizarTabla() {
        if (tbvCooperativa.getItems() == null || !tbvCooperativa.getItems().stream().anyMatch(a -> a.id.get().equalsIgnoreCase(coopDto.getId().toString()))) {
            coopDto.setModificado(true);
            tbvCooperativa.getItems().add(coopDto);
            tbvCooperativa.refresh();
        }
    }

    private void cargarCooperativa(Long id) {
        CooperativaService service = new CooperativaService();
        Respuesta respuesta = service.getCooperativa(id);

        if (respuesta.getEstado()) {
            unbindCooperativa();
            coopDto = (CooperativaDto) respuesta.getResultado("Cooperativa");
            bindCooperativa(false);
            validarRequeridos();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Cooperativa", getStage(), respuesta.getMensaje());
        }
    }

    private class ButtonCell extends TableCell<CooperativaDto, Boolean> {

        final Button cellButton = new Button();

        ButtonCell() {
            cellButton.setPrefWidth(500);
            cellButton.getStyleClass().add("jfx-btnimg-tbvseleccionar");
            cellButton.setOnAction((event) -> {
                CooperativaDto coo = ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                AppContext.getInstance().set("cooperativa", coo);
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar Cooperativa", getStage(),
                        "Cooperativa Seleccionada");

            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty); //To change body of generated methods, choose Tools | Templates.
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    @Override
    public void initialize() {

    }
}
