package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.cooperativafe.util.FlowController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;

public class PrincipalViewController extends Controller implements Initializable {

    @FXML
    private BorderPane root;
    @FXML
    private JFXButton btnSalir;
    @FXML
    private JFXButton btnProfesores;
    @FXML
    private JFXButton btnFuncionarios;
    @FXML
    private JFXButton btnAsociados;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @Override
    public void initialize() {
    }

    @FXML
    private void onActionbtnSalir(ActionEvent event) {
        FlowController.getInstance().goMain();
    }

    @FXML
    private void onActionBtnProfesores(ActionEvent event) {
        FlowController.getInstance().cleanView("PrincipalView", "Center");
        FlowController.getInstance().goView("MenuProfesorView", "Left", null);
    }

    @FXML
    private void onActionBtnFuncionarios(ActionEvent event) {
        FlowController.getInstance().cleanView("PrincipalView", "Center");
        FlowController.getInstance().goView("MenuFuncionarioView", "Left", null);
    }

    @FXML
    private void onActionBtnAsociados(ActionEvent event) {
        FlowController.getInstance().goView("AsociadosView");
        FlowController.getInstance().initialize();
    }
}
