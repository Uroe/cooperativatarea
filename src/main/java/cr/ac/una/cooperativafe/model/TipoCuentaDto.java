package cr.ac.una.cooperativafe.model;

import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Navarro
 */
public class TipoCuentaDto {

    public SimpleStringProperty id;
    public SimpleStringProperty nomCuenta;
    public SimpleStringProperty plazoTiempo;
    public SimpleStringProperty ultimoRetiro;
    public SimpleStringProperty ultimoDeposito;
    public SimpleStringProperty saldo;
    public ObjectProperty<LocalDate> fechaRetiro;
    public ObjectProperty<LocalDate> fechaDeposito;
    public SimpleBooleanProperty estado;
    private Boolean modificado;
    public CooperativaDto Coop;
    ObservableList<AsociadoDto> asociados;

    public TipoCuentaDto() {
        this.modificado = false;
        this.id = new SimpleStringProperty();
        this.nomCuenta = new SimpleStringProperty();
        this.plazoTiempo = new SimpleStringProperty();
        this.ultimoRetiro = new SimpleStringProperty();
        this.ultimoDeposito = new SimpleStringProperty();
        this.saldo = new SimpleStringProperty();
        this.fechaRetiro = new SimpleObjectProperty();
        this.fechaDeposito = new SimpleObjectProperty();
        this.estado = new SimpleBooleanProperty(true);
        asociados = FXCollections.observableArrayList();
        this.Coop = new CooperativaDto();
        
    }

    public TipoCuentaDto(TipoCuenta tipoCuenta) {
        this();
        this.id.set(tipoCuenta.getId().toString());
        this.nomCuenta.set(tipoCuenta.getNombre());
        this.plazoTiempo.set(tipoCuenta.getPlazoTiempo());
        this.ultimoRetiro.set(tipoCuenta.getUltimoRetiro().toString());
        this.ultimoDeposito.set(tipoCuenta.getUltimoDeposito().toString());
        this.saldo.set(tipoCuenta.getSaldo().toString());
        this.fechaRetiro.set(tipoCuenta.getFechaRetiro());
        this.fechaDeposito.set(tipoCuenta.getFechaDeposito());
        this.estado.set(tipoCuenta.getEstado().equalsIgnoreCase("A"));
    }

    public ObservableList<AsociadoDto> getAsociados() {
        return asociados;
    }

    public void setAsociados(ObservableList<AsociadoDto> asociados) {
        this.asociados = asociados;
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getNomCuenta() {
        return nomCuenta.get();
    }

    public void setNomCuenta(String nomCuenta) {
        this.nomCuenta.set(nomCuenta);
    }

    public String getPlazoTiempo() {
        return plazoTiempo.get();
    }

    public void setPlazoTiempo(String plazoTiempo) {
        this.plazoTiempo.set(plazoTiempo);
    }

    public String getUltimoRetiro() {
        return ultimoRetiro.get();
    }

//      public Integer getUltimoRetiro() {
//          if(ultimoRetiro.get().isEmpty()||ultimoRetiro.get()==null){
//              return 0;
//          }else{
//               return Integer.parseInt(ultimoRetiro.get());
//          }
//    }
    public void setUltimoRetiro(String ultimoRetiro) {
        this.ultimoRetiro.set(ultimoRetiro);
    }

    public String getUltimoDeposito() {
        return ultimoDeposito.get();
    }

    public void setUltimoDeposito(String ultimoDeposito) {
        this.ultimoDeposito.set(ultimoDeposito);
    }

    public String getSaldo() {
        return saldo.get();
    }

    public void setSaldo(String saldo) {
        this.saldo.set(saldo);
    }

    public LocalDate getFechaRetiro() {
        return fechaRetiro.get();
    }

    public void setFechaRetiro(LocalDate fechaRetiro) {
        this.fechaRetiro.set(fechaRetiro);
    }

    public LocalDate getFechaDeposito() {
        return fechaDeposito.get();
    }

    public void setFechaDeposito(LocalDate fechaDeposito) {
        this.fechaDeposito.set(fechaDeposito);
    }

    public String getEstado() {
        return estado.getValue() ? "A" : "I";
    }

    public void setEstado(String estado) {
        this.estado.setValue(estado.equalsIgnoreCase("A"));
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public CooperativaDto getCoop() {
        return Coop;
    }

    public void setCoop(CooperativaDto Coop) {
        this.Coop = Coop;
    }
}
