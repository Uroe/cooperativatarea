package cr.ac.una.cooperativafe.model;

import cr.ac.una.cooperativafe.model.TipoCuenta;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2020-06-02T13:36:39", comments="EclipseLink-2.7.4.v20190115-rNA")
@StaticMetamodel(Cooperativa.class)
public class Cooperativa_ { 

    public static volatile SingularAttribute<Cooperativa, String> estado;
    public static volatile SingularAttribute<Cooperativa, String> nombreCoop;
    public static volatile SingularAttribute<Cooperativa, String> nombreProf;
    public static volatile SingularAttribute<Cooperativa, TipoCuenta> tipoCuenta;
    public static volatile SingularAttribute<Cooperativa, Long> id;
    public static volatile SingularAttribute<Cooperativa, String> anoLectivo;
    public static volatile SingularAttribute<Cooperativa, String> nombreEsc;
    public static volatile SingularAttribute<Cooperativa, Long> version;

}