package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativafe.model.AsociadoDto;
import cr.ac.una.cooperativafe.service.AsociadoService;
import cr.ac.una.cooperativafe.util.AppContext;
import cr.ac.una.cooperativafe.util.BindingUtils;
import cr.ac.una.cooperativafe.util.Formato;
import cr.ac.una.cooperativafe.util.Mensaje;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ControlAsociadosViewController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtFolio;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private ImageView imgFoto;
    @FXML
    private JFXRadioButton rbdMasculino;
    @FXML
    private ToggleGroup tggGenero;
    @FXML
    private JFXRadioButton rbdFemenino;
    @FXML
    private JFXTextField txtEdad;
    @FXML
    private JFXTextField txtPapellido;
    @FXML
    private JFXTextField txtSapellido;
    @FXML
    private JFXTextField txtGrado;

    Image image;
    AsociadoDto asociado;
    List<Node> requeridos = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        rbdMasculino.setUserData("M");
        rbdFemenino.setUserData("F");
        txtFolio.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtPapellido.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtSapellido.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtEdad.setTextFormatter(Formato.getInstance().integerFormat());
        txtGrado.setTextFormatter(Formato.getInstance().integerFormat());
        asociado = new AsociadoDto();
        if (AppContext.getInstance().get("Asociado") != null) {
            asociado = (AsociadoDto) AppContext.getInstance().get("Asociado");
            cargarAso();
        } else {
            nuevoAsociado();
            indicarRequeridos();
        }
    }

    private void bindAsociado(Boolean nuevo) {
        if (!nuevo) {
            txtFolio.textProperty().bind(asociado.asoFolio);
        }
        txtNombre.textProperty().bindBidirectional(asociado.asoNombre);
        txtPapellido.textProperty().bindBidirectional(asociado.asoPapellido);
        txtSapellido.textProperty().bindBidirectional(asociado.asoSapellido);
        txtEdad.textProperty().bindBidirectional(asociado.asoEdad);
        txtGrado.textProperty().bindBidirectional(asociado.asoGrado);
        BindingUtils.bindToggleGroupToProperty(tggGenero, asociado.asoGenero);
    }

    private void unbindAsociado() {
        txtFolio.textProperty().unbind();
        txtNombre.textProperty().unbindBidirectional(asociado.asoNombre);
        txtPapellido.textProperty().unbindBidirectional(asociado.asoPapellido);
        txtSapellido.textProperty().unbindBidirectional(asociado.asoSapellido);
        txtEdad.textProperty().unbindBidirectional(asociado.asoEdad);
        txtGrado.textProperty().unbindBidirectional(asociado.asoGrado);
        BindingUtils.unbindToggleGroupToProperty(tggGenero, asociado.asoGenero);
    }

    private void nuevoAsociado() {
        imgFoto.setVisible(false);
        unbindAsociado();
        asociado = new AsociadoDto();
        bindAsociado(true);
        txtFolio.clear();
        txtFolio.requestFocus();
    }

    private void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre, txtPapellido));
    }

    public String validarRequeridos() {
        boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && ((JFXTextField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }

    }

    @FXML
    private void onKeyPressedtxtFolio(KeyEvent event) throws IOException, ClassNotFoundException {
        if (event.getCode() == KeyCode.ENTER && !txtFolio.getText().isEmpty()) {
            cargarAsociado(Long.valueOf(txtFolio.getText()));
        }
    }

    private void cargarAsociado(Long folio) throws IOException, ClassNotFoundException {
        AsociadoService service = new AsociadoService();
        Respuesta respuesta = service.getAsociado(folio);

        if (respuesta.getEstado()) {
            unbindAsociado();
            asociado = (AsociadoDto) respuesta.getResultado("Asociado");
            bindAsociado(false);
            validarRequeridos();
            AppContext.getInstance().set("Asociado", asociado);
            ByteArrayInputStream bis = new ByteArrayInputStream(asociado.asoFoto);
            image = new Image(bis);
            imgFoto.setImage(image);
            imgFoto.setVisible(true);
            
            AppContext.getInstance().set("asociado", asociado);

        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar asociado", getStage(), respuesta.getMensaje());
        }
    }

    private void cargarAso() {
        unbindAsociado();
        bindAsociado(false);
        validarRequeridos();
        ByteArrayInputStream bis = new ByteArrayInputStream(asociado.asoFoto);
        image = new Image(bis);
        imgFoto.setImage(image);
        imgFoto.setVisible(true);
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Asociado", getStage(), "¿Esta seguro que desea limpiar el registro?")) {
            AppContext.getInstance().delete("Asociado");
            nuevoAsociado();
            AppContext.getInstance().delete("asociado");
        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar empleado", getStage(), invalidos);
            } else {
                AsociadoService service = new AsociadoService();
                Respuesta respuesta = service.guardarAsociado(asociado);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar asociado", getStage(), respuesta.getMensaje());
                } else {
                    unbindAsociado();
                    asociado = (AsociadoDto) respuesta.getResultado("Asociado");
                    bindAsociado(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar asociado", getStage(),
                            "Asociado actualizado correctamente");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ControlAsociadosViewController.class.getName()).log(Level.SEVERE, "Error guardando el asociado", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar asociado", getStage(),
                    "Ocurrio un error guardando el asociado");
        }
    }

    @FXML
    private void onActionbtnEliminar(ActionEvent event) {
        try {
            if (asociado.getAsoFolio() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar asociado",
                        getStage(), "Debe cargar el asociado que desea eliminar");
            } else {
                AsociadoService service = new AsociadoService();
                Respuesta respuesta = service.eliminarAsociado(asociado.getAsoFolio());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar asociado", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar asociado", getStage(),
                            "Empleado eliminado correctamente");
                    nuevoAsociado();
                    imgFoto.setVisible(false);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ControlAsociadosViewController.class.getName()).log(Level.SEVERE, "Error eliminando el asociado", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar asociado", getStage(),
                    "Ocurrio un error eliminando el asociado");
        }
    }

    @Override
    public void initialize() {

    }

}
