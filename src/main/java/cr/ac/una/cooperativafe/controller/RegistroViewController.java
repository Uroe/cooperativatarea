package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativafe.PanelCamara;
import cr.ac.una.cooperativafe.model.AsociadoDto;
import cr.ac.una.cooperativafe.service.AsociadoService;
import cr.ac.una.cooperativafe.util.FlowController;
import cr.ac.una.cooperativafe.util.Formato;
import cr.ac.una.cooperativafe.util.Mensaje;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javax.imageio.ImageIO;

public class RegistroViewController extends Controller implements Initializable {

    @FXML
    private JFXButton btnRegistrarse;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXButton btnFoto;
    @FXML
    private JFXTextField txtPapellido;
    @FXML
    private JFXTextField txtGrado;
    @FXML
    private JFXTextField txtEdad;

    File foto;
    String ruta;
    AsociadoDto asociado;
    List<Node> requeridos = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtPapellido.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtGrado.setTextFormatter(Formato.getInstance().integerFormat());
        txtEdad.setTextFormatter(Formato.getInstance().integerFormat());
        asociado = new AsociadoDto();
        nuevoAsociado();
        indicarRequeridos();
    }

    private void bindAsociado() {
        txtNombre.textProperty().bindBidirectional(asociado.asoNombre);
        txtPapellido.textProperty().bindBidirectional(asociado.asoPapellido);
        txtEdad.textProperty().bindBidirectional(asociado.asoEdad);
        txtGrado.textProperty().bindBidirectional(asociado.asoGrado);
    }

    private void unbindAsociado() {
        txtNombre.textProperty().unbindBidirectional(asociado.asoNombre);
        txtPapellido.textProperty().unbindBidirectional(asociado.asoPapellido);
        txtEdad.textProperty().unbindBidirectional(asociado.asoEdad);
        txtGrado.textProperty().unbindBidirectional(asociado.asoGrado);

    }

    private void nuevoAsociado() {
        unbindAsociado();
        asociado = new AsociadoDto();
        bindAsociado();

    }

    private void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre, txtPapellido));
    }

    public String validarRequeridos() {
        boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && ((JFXTextField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }

    }

    @FXML
    private void onActionbtnTomarFoto(ActionEvent event) {
        PanelCamara camara = new PanelCamara();
        camara.setBounds(300, 400, 400, 300);
        camara.setVisible(true);
    }

    @FXML
    private void onActionbtnRegistrarse(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Registro", getStage(), invalidos);
            } else {

                foto = new File("Foto_Asociado.png");
                ruta = foto.getAbsolutePath();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                BufferedImage image = ImageIO.read(foto);
                ImageIO.write(image, "png", bos);
                asociado.asoFoto = bos.toByteArray();

                AsociadoService service = new AsociadoService();
                Respuesta respuesta = service.guardarAsociado(asociado);

                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Registro", getStage(), respuesta.getMensaje());
                } else {
                    unbindAsociado();
                    asociado = (AsociadoDto) respuesta.getResultado("Asociado");
                    bindAsociado();
                    foto.delete();

                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Registro", getStage(),
                            "Asociado registrado correctamente" + "\nFolio generado: " + asociado.getAsoFolio());
                    FlowController.getInstance().goView("AsociadosView");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ControlAsociadosViewController.class.getName()).log(Level.SEVERE, "Registro", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Registro", getStage(),
                    "Ocurrio un error registrando el asociado");
        }
    }

    @Override
    public void initialize() {

    }
}
