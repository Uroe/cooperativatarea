package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativafe.model.CooperativaDto;
import cr.ac.una.cooperativafe.model.TipoCuentaDto;
import cr.ac.una.cooperativafe.service.TipoCuentaService;
import cr.ac.una.cooperativafe.util.AppContext;
import cr.ac.una.cooperativafe.util.Formato;
import cr.ac.una.cooperativafe.util.Mensaje;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class TipoCuentaViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXTextField txtNombreCuenta;
    @FXML
    private JFXTextField txtPlazoTiempo;
    @FXML
    private JFXCheckBox chkActivo;
    @FXML
    private TableView<TipoCuentaDto> tbvTipoCuentas;
    @FXML
    private TableColumn<TipoCuentaDto, String> tbcId;
    @FXML
    private TableColumn<TipoCuentaDto, String> tbcNomCuenta;
    @FXML
    private TableColumn<TipoCuentaDto, String> tbcPlazoTiempo;
    @FXML
    private Label labelNomCoop;
    @FXML
    private JFXButton btnNuevo;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnGuardar;
    TipoCuentaDto tipoCuentaDto;
    List<Node> requeridos = new ArrayList<>();
    CooperativaDto coope;

    TipoCuentaService service = new TipoCuentaService();
    List<TipoCuentaDto> listaTabla = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtNombreCuenta.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtPlazoTiempo.setTextFormatter(Formato.getInstance().maxLengthFormat(10));
        tipoCuentaDto = new TipoCuentaDto();
        nuevoTipoCuenta();
        indicarRequeridos();

        coope = (CooperativaDto) AppContext.getInstance().get("cooperativa");
        labelNomCoop.setText(coope.getNomCooperativa());

        Respuesta r = service.getTipoCuentas(coope.getId());
        if (r.getEstado()) {
            listaTabla = (List<TipoCuentaDto>) r.getResultado("TipoCuenta");
            tbvTipoCuentas.getItems().addAll(listaTabla);
            tbvTipoCuentas.refresh();
        }

        tbcId.setCellValueFactory(cd -> cd.getValue().id);
        tbcNomCuenta.setCellValueFactory(cd -> cd.getValue().nomCuenta);
        tbcPlazoTiempo.setCellValueFactory(cd -> cd.getValue().plazoTiempo);

        tbvTipoCuentas.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends TipoCuentaDto> observable, TipoCuentaDto oldValue, TipoCuentaDto newValue) -> {
            unbindTipoCuenta();
            if (newValue != null) {
                tipoCuentaDto = newValue;
                bindTipoCuenta(false);
            }
        });
    }

    private void bindTipoCuenta(Boolean nuevo) {
        if (!nuevo) {
            txtId.textProperty().bind(tipoCuentaDto.id);
        }
        txtNombreCuenta.textProperty().bindBidirectional(tipoCuentaDto.nomCuenta);
        txtPlazoTiempo.textProperty().bindBidirectional(tipoCuentaDto.plazoTiempo);
        chkActivo.selectedProperty().bindBidirectional(tipoCuentaDto.estado);
    }

    private void unbindTipoCuenta() {
        txtId.textProperty().unbind();
        txtNombreCuenta.textProperty().unbindBidirectional(tipoCuentaDto.nomCuenta);
        txtPlazoTiempo.textProperty().unbindBidirectional(tipoCuentaDto.plazoTiempo);
        chkActivo.selectedProperty().unbindBidirectional(tipoCuentaDto.estado);
    }

    private void nuevoTipoCuenta() {
        unbindTipoCuenta();
        tbvTipoCuentas.getSelectionModel().select(null);
        tipoCuentaDto = new TipoCuentaDto();
        bindTipoCuenta(true);
        txtId.clear();
        txtId.requestFocus();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombreCuenta, txtPlazoTiempo));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = " ";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && ((JFXTextField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && ((JFXPasswordField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato (" + invalidos + ").";
        }
    }

    @FXML
    private void onKeyPressedTxtId(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtId.getText().isEmpty()) {
            cargarTipoCuenta(Long.valueOf(txtId.getText()));
        }
    }

    @FXML
    private void onActionBntNuevo(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar tipo de Cuenta", getStage(), "Esta seguro que desea limpiar el registro?")) {
            nuevoTipoCuenta();
        }
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        try {
            if (tipoCuentaDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar tipo de cuenta", getStage(), "Dede cargar el tipo de cuenta que desea eliminar");
            } else {
                TipoCuentaService service = new TipoCuentaService();
                Respuesta respuesta = service.eliminarTipoCuenta(tipoCuentaDto.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar tipo de cuenta", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar tipo de cuenta", getStage(), "Tipo de cuenta eliminada correctamente");
                    nuevoTipoCuenta();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TipoCuentaViewController.class.getName()).log(Level.SEVERE, "Error eliminando el tipo de cuenta.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar tipo de cuenta", getStage(), "Ocurrio un error eliminando el tipo de cuenta");
        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar tipo de cuenta", getStage(), invalidos);
            } else {
                TipoCuentaService service = new TipoCuentaService();
                tipoCuentaDto.setCoop(coope);
                Respuesta respuesta = service.guardarTipoCuenta(tipoCuentaDto);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar tipo de cuenta", getStage(), respuesta.getMensaje());
                } else {
                    unbindTipoCuenta();
                    tipoCuentaDto = (TipoCuentaDto) respuesta.getResultado("TipoCuenta");
                    bindTipoCuenta(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar tipo de cuenta", getStage(), "Tipo de cuenta guardada correctamente");
                    actualizarTabla();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TipoCuentaViewController.class.getName()).log(Level.SEVERE, "Error guardando el tipo de cuenta.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar tipo de cuenta", getStage(), "Ocurrio un error guardando el tipo de cuenta");
        }
    }

    private void actualizarTabla() {
        if (tbvTipoCuentas.getItems() == null || !tbvTipoCuentas.getItems().stream().anyMatch(a -> a.id.get().equalsIgnoreCase(tipoCuentaDto.getId().toString()))) {
            tipoCuentaDto.setModificado(true);
            tbvTipoCuentas.getItems().add(tipoCuentaDto);
            tbvTipoCuentas.refresh();
        }
    }

    private void cargarTipoCuenta(Long id) {
        TipoCuentaService service = new TipoCuentaService();
        Respuesta respuesta = service.getTipoCuenta(id);

        if (respuesta.getEstado()) {
            unbindTipoCuenta();
            tipoCuentaDto = (TipoCuentaDto) respuesta.getResultado("TipoCuenta");
            bindTipoCuenta(false);
            validarRequeridos();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Tipo de Cuenta", getStage(), respuesta.getMensaje());
        }
    }

    @Override
    public void initialize() {

    }
}
