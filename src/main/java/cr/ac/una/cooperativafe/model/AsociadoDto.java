package cr.ac.una.cooperativafe.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.bind.annotation.XmlTransient;

public class AsociadoDto {

    public SimpleStringProperty asoFolio;
    public SimpleStringProperty asoNombre;
    public byte[] asoFoto;
    public SimpleStringProperty asoPapellido;
    public SimpleStringProperty asoSapellido;
    public SimpleStringProperty asoEdad;
    public SimpleStringProperty asoGrado;
    public ObjectProperty<String> asoGenero;
    ObservableList<TipoCuentaDto> cuentas;

    @XmlTransient
    private Boolean modificado;

    public AsociadoDto() {
        this.modificado = false;
        this.asoFolio = new SimpleStringProperty();
        this.asoNombre = new SimpleStringProperty();
        this.asoFoto = new byte[100];
        this.asoPapellido = new SimpleStringProperty();
        this.asoSapellido = new SimpleStringProperty();
        this.asoEdad = new SimpleStringProperty();
        this.asoGrado = new SimpleStringProperty();
        this.asoGenero = new SimpleObjectProperty<>("M");
        cuentas = FXCollections.observableArrayList();
    }

    public AsociadoDto(Long folio, String nombre) {
        this();
        setAsoFolio(folio);
        setAsoNombre(nombre);
    }

    public AsociadoDto(Asociado asociado) {
        this();
        this.asoFolio.set(asociado.getFolio().toString());
        this.asoNombre.set(asociado.getNombre());
        this.asoFoto = (byte[]) asociado.getFoto();
        this.asoPapellido.set(asociado.getpApellido());
        this.asoSapellido.set(asociado.getsApellido());
        this.asoEdad.set(asociado.getEdad().toString());
        this.asoGrado.set(asociado.getGrado().toString());
        this.asoGenero.set(asociado.getGenero());
    }

    public Long getAsoFolio() {
        if (asoFolio.get() != null && !asoFolio.get().isEmpty()) {
            return Long.valueOf(asoFolio.get());
        } else {
            return null;
        }
    }

    public ObservableList<TipoCuentaDto> getCuentas() {
        return cuentas;
    }
    

    public void setAsoFolio(Long asoFolio) {
        this.asoFolio.set(asoFolio.toString());
    }

    public String getAsoNombre() {
        return asoNombre.get();
    }

    public void setAsoNombre(String asoNombre) {
        this.asoNombre.set(asoNombre);
    }

    public String getAsoPapellido() {
        return asoPapellido.get();
    }

    public void setAsoPapellido(String asoApellido) {
        this.asoPapellido.set(asoApellido);
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public void setAsoSapellido(String asoSapellido) {
        this.asoSapellido.set(asoSapellido);
    }

    public void setAsoEdad(String asoEdad) {
        this.asoEdad.set(asoEdad);
    }

    public void setAsoGrado(String asoGrado) {
        this.asoGrado.set(asoGrado);
    }

    public void setAsoGenero(String asoGenero) {
        this.asoGenero.set(asoGenero);
    }

    public byte[] getAsoFoto() {
        return asoFoto;
    }

    public void setAsoFoto(byte[] asoFoto) {
        this.asoFoto = asoFoto;
    }

    public String getAsoSapellido() {
        return asoSapellido.get();
    }

    public String getAsoEdad() {
        return asoEdad.get();
    }

    public String getAsoGrado() {
        return asoGrado.get();
    }

    public String getAsoGenero() {
        return asoGenero.get();
    }

}
