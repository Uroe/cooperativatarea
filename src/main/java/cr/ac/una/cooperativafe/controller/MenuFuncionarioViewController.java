/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.cooperativafe.util.AppContext;
import cr.ac.una.cooperativafe.util.FlowController;
import cr.ac.una.cooperativafe.util.Mensaje;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class MenuFuncionarioViewController extends Controller implements Initializable {

    @FXML
    private JFXButton btnAsociados;
    @FXML
    private JFXButton btnCarnets;
    @FXML
    private JFXButton btnCuentas;
    @FXML
    private JFXButton btnDepositos;
    @FXML
    private JFXButton btnRetiros;
    @FXML
    private AnchorPane root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void onActionBtnAsociados(ActionEvent event) {
        FlowController.getInstance().goView("ControlAsociadosView");
    }

    @FXML
    private void onActionBtnCarnets(ActionEvent event) {

        if (AppContext.getInstance().get("asociado") != null && AppContext.getInstance().get("cooperativa") != null  ) {
            FlowController.getInstance().goView("CarnetView");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Creacion de carnet", getStage(),
                    "Para crear un carnet primero debe seleccionar una cooperativa y un asociado.");
        }
    }

    @FXML
    private void onActionBtnCuentas(ActionEvent event) {
        if (AppContext.getInstance().get("cooperativa") != null) {
            FlowController.getInstance().goView("CuentasView");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Apertura de Cuentas", getStage(),
                    "Para aperturar una cuenta primero debe seleccionar una cooperativa");
        }

    }

    @FXML
    private void onActionBtnDepositos(ActionEvent event) {
        FlowController.getInstance().goView("DepositosView");
    }

    @FXML
    private void onActionBtnRetiros(ActionEvent event) {
        FlowController.getInstance().goView("RetirosView");
        FlowController.getInstance().initialize();
        
    }

    @Override
    public void initialize() {

    }
}
