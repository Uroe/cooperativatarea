
package cr.ac.una.cooperativafe.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "COOPFE_ASOCIADOS", schema = "una")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asociado.findAll", query = "SELECT a FROM Asociado a"),
    @NamedQuery(name = "Asociado.findByAsoFolio", query = "SELECT a FROM Asociado a WHERE a.folio = :folio")})
public class Asociado implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "COOPFE_ASOCIADOS_ASO_FOLIO_GENERATOR", sequenceName = "una.COOPFE_ASOCIADOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COOPFE_ASOCIADOS_ASO_FOLIO_GENERATOR")
    @Basic(optional = false)
    @Column(name = "ASO_FOLIO")
    private Long folio;
    @Basic(optional = false)
    @Lob
    @Column(name = "ASO_FOTO")
    private byte[] foto;
    @Basic(optional = false)
    @Column(name = "ASO_NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "ASO_PAPELLIDO")
    private String pApellido;
    @Column(name = "ASO_SAPELLIDO")
    private String sApellido;
    @Column(name = "ASO_EDAD")
    private Integer edad;
    @Column(name = "ASO_GRADO")
    private Integer grado;
    @Column(name = "ASO_GENERO")
    private String genero;
    @Version
    @Basic(optional = false)
    @Column(name = "ASO_VERSION")
    private Long version;
    @ManyToMany(mappedBy = "asociados", fetch = FetchType.LAZY)
    private List<TipoCuenta> tipocuentas;

    public Asociado() {
        this.version = Long.valueOf(1);
    }

    public Asociado(Long folio) {
        this.folio = folio;
        this.version = Long.valueOf(1);
    }

    public Asociado(Long folio, byte[] foto, String nombre, String pApellido, Integer edad, Integer grado, Long version) {
        this.folio = folio;
        this.foto = foto;
        this.nombre = nombre;
        this.pApellido = pApellido;
        this.edad = edad;
        this.grado = grado;
        this.version = Long.valueOf(1);
    }

    public Asociado(AsociadoDto asociadoDto) {
        this.folio = asociadoDto.getAsoFolio();
        actualizarAsociado(asociadoDto);
    }

    public void actualizarAsociado(AsociadoDto asociadoDto) {
        this.nombre = asociadoDto.getAsoNombre();
        this.foto = asociadoDto.getAsoFoto();
        this.pApellido = asociadoDto.getAsoPapellido();
        this.sApellido = asociadoDto.getAsoSapellido();
        this.edad = Integer.valueOf(asociadoDto.getAsoEdad());
        this.grado = Integer.valueOf(asociadoDto.getAsoGrado());
        this.genero = asociadoDto.getAsoGenero();
    }

    public Long getFolio() {
        return folio;
    }

    public void setFolio(Long folio) {
        this.folio = folio;
    }

    public Serializable getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getpApellido() {
        return pApellido;
    }

    public void setpApellido(String pApellido) {
        this.pApellido = pApellido;
    }

    public String getsApellido() {
        return sApellido;
    }

    public void setsApellido(String sApellido) {
        this.sApellido = sApellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getGrado() {
        return grado;
    }

    public void setGrado(Integer grado) {
        this.grado = grado;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @XmlTransient
    public List<TipoCuenta> getTipocuentas() {
        return tipocuentas;
    }

    public void setTipocuentas(List<TipoCuenta> tipocuentas) {
        this.tipocuentas = tipocuentas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folio != null ? folio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asociado)) {
            return false;
        }
        Asociado other = (Asociado) object;
        if ((this.folio == null && other.folio != null) || (this.folio != null && !this.folio.equals(other.folio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.cooperativafe.model.Asociado[ asoFolio=" + folio + " ]";
    }

}
