/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.cooperativafe.model;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Navarro
 */
public class CooperativaDto {
    
    public SimpleStringProperty id;
    public SimpleStringProperty nomCooperativa;
    public SimpleStringProperty nomEscuela;
    public SimpleStringProperty nomProfesor;
    public SimpleStringProperty anoLectivo;
    public SimpleBooleanProperty estado;
    private Boolean modificado;

    public CooperativaDto() {
        this.modificado = false;
        this.id = new SimpleStringProperty();
        this.nomCooperativa = new SimpleStringProperty();
        this.nomEscuela = new SimpleStringProperty();
        this.nomProfesor = new SimpleStringProperty();
        this.anoLectivo = new SimpleStringProperty();
        this.estado = new SimpleBooleanProperty(true);
    }
    
    public CooperativaDto(Cooperativa coop) {
        this();
        this.id.set(coop.getId().toString());
        this.nomCooperativa.set(coop.getNombreCoop());
        this.nomEscuela.set(coop.getNombreEsc());
        this.nomProfesor.set(coop.getNombreProf());
        this.anoLectivo.set(coop.getAnoLectivo());
        this.estado.set(coop.getEstado().equalsIgnoreCase("A"));
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getNomCooperativa() {
        return nomCooperativa.get();
    }

    public void setNomCooperativa(String nomCooperativa) {
        this.nomCooperativa.set(nomCooperativa);
    }

    public String getNomEscuela() {
        return nomEscuela.get();
    }

    public void setNomEscuela(String nomEscuela) {
        this.nomEscuela.set(nomEscuela);
    }

    public String getNomProfesor() {
        return nomProfesor.get();
    }

    public void setNomProfesor(String nomProfesor) {
        this.nomProfesor.set(nomProfesor);
    }

    public String getAnoLectivo() {
        return anoLectivo.get();
    }

    public void setAnoLectivo(String anoLectivo) {
        this.anoLectivo.set(anoLectivo);
    }

    public String getEstado() {
        return estado.getValue() ? "A" : "I";
    }

    public void setEstado(String estado) {
        this.estado.setValue(estado.equalsIgnoreCase("A"));
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }
    
    @Override
    public String toString() {
        return "CooperativaDto{" + "id=" + id + ", nomCooperativa=" + nomCooperativa + ", nomEscuela=" + nomEscuela + ", nomProfesor=" + nomProfesor + ", anoLectivo=" + anoLectivo + ", estado=" + estado + '}';
    }
    
    
}
