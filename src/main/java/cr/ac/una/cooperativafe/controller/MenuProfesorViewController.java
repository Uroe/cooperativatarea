/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.cooperativafe.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.cooperativafe.util.AppContext;
import cr.ac.una.cooperativafe.util.FlowController;
import cr.ac.una.cooperativafe.util.Mensaje;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class MenuProfesorViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXButton btnCooperativa;
    @FXML
    private JFXButton btnTipoCuenta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void onActionBtnCooperativa(ActionEvent event) {
        FlowController.getInstance().goView("CooperativaView");
    }

    @FXML
    private void onActionBtnTipoCuenta(ActionEvent event) {
        if (AppContext.getInstance().get("cooperativa") != null) {
            FlowController.getInstance().goView("TipoCuentaView");
            FlowController.getInstance().initialize();
        } else{
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar TipoCuenta", getStage(),
                    "Aun no ha seleccionado una cooperativa");
        }

    }

    @Override
    public void initialize() {

    }

}
