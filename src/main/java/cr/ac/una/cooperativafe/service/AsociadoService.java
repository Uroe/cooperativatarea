package cr.ac.una.cooperativafe.service;

import cr.ac.una.cooperativafe.model.Asociado;
import cr.ac.una.cooperativafe.model.AsociadoDto;
import cr.ac.una.cooperativafe.model.TipoCuenta;
import cr.ac.una.cooperativafe.model.TipoCuentaDto;
import cr.ac.una.cooperativafe.util.EntityManagerHelper;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class AsociadoService {

    EntityManager em = EntityManagerHelper.getManager();
    private EntityTransaction et;

    public Respuesta getAsociado(Long folio) {
        try {
            Query qryAsociado = em.createNamedQuery("Asociado.findByAsoFolio", Asociado.class);
            qryAsociado.setParameter("folio", folio);

            Asociado asociado = (Asociado) qryAsociado.getSingleResult();
            AsociadoDto asociadoDto = new AsociadoDto(asociado);
            for (TipoCuenta tcuen : asociado.getTipocuentas()) {
                asociadoDto.getCuentas().add(new TipoCuentaDto(tcuen));
            }
            return new Respuesta(true, "", "", "Asociado", asociadoDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, "Error obteniendo el asociado.", "getAsociado " + ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(AsociadoService.class.getName()).log(Level.SEVERE, "Error obteniendo el asociado [" + folio + "]", ex);
            return new Respuesta(false, "Error obteniendo el asociado.", "getAsociado " + ex.getMessage());
        }

    }
    
    
    public Respuesta guardarAsociado(AsociadoDto asociadoDto) {
        try {
            et = em.getTransaction();
            et.begin();
            Asociado asociado;
            if (asociadoDto.getAsoFolio() != null && asociadoDto.getAsoFolio() > 0) {
                asociado = em.find(Asociado.class, asociadoDto.getAsoFolio());
                if (asociado == null) {
                    et.rollback();
                    return new Respuesta(false, "No se encontro el asociado a modificar.",
                            "guardarAsociado NoResultException");
                }
                asociado.actualizarAsociado(asociadoDto);
                //asociado = em.merge(asociado);
                if (!asociadoDto.getCuentas().isEmpty()) {
                    for (TipoCuentaDto tcd : asociadoDto.getCuentas()) {
                        if (tcd.getModificado()) {
                            TipoCuenta cuenta = em.find(TipoCuenta.class, tcd.getId());
                            cuenta.getAsociados().add(asociado);
                            asociado.getTipocuentas().add(cuenta);
                        }
                    }
                }
                asociado = em.merge(asociado);
            } else {
                asociado = new Asociado(asociadoDto);
                em.persist(asociado);
            }
            et.commit();
            return new Respuesta(true, "", "", "Asociado", new AsociadoDto(asociado));
        } catch (Exception ex) {
            et.rollback();
            Logger.getLogger(AsociadoService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar el asociado.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar el asociado.", "guardarAsociado " + ex.getMessage());
        }
    }

    public Respuesta eliminarAsociado(Long folio) {
        try {
            et = em.getTransaction();
            et.begin();
            Asociado asociado;
            if (folio != null && folio > 0) {
                asociado = em.find(Asociado.class, folio);
                if (asociado == null) {
                    et.rollback();
                    return new Respuesta(false, "No se encontro el asociado a eliminar", "eliminarAsociado NoResultException");
                }
                em.remove(asociado);
            } else {
                et.rollback();
                return new Respuesta(false, "Debe cargar el asociado a eliminar", "eliminarAsociado NoResultException");
            }
            et.commit();
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            et.rollback();
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, "No se puede eliminar el asociado porque tiene relaciones con otros registros",
                        "eliminarAsociado" + ex.getMessage());
            }
            Logger.getLogger(AsociadoService.class.getName()).log(Level.SEVERE,
                    "Ocurrio un error al eliminar el asociado", ex);
            return new Respuesta(false, "Ocurrio un error al eliminar el asociado.", "eliminarAsociado" + ex.getMessage());
        }
    }
}
