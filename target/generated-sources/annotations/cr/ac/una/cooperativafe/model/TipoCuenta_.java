package cr.ac.una.cooperativafe.model;

import cr.ac.una.cooperativafe.model.Asociado;
import cr.ac.una.cooperativafe.model.Cooperativa;
import java.time.LocalDate;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2020-06-02T13:36:39", comments="EclipseLink-2.7.4.v20190115-rNA")
@StaticMetamodel(TipoCuenta.class)
public class TipoCuenta_ { 

    public static volatile SingularAttribute<TipoCuenta, String> estado;
    public static volatile SingularAttribute<TipoCuenta, Cooperativa> tcuenIdcoop;
    public static volatile ListAttribute<TipoCuenta, Asociado> asociados;
    public static volatile SingularAttribute<TipoCuenta, Integer> ultimoDeposito;
    public static volatile SingularAttribute<TipoCuenta, Integer> ultimoRetiro;
    public static volatile SingularAttribute<TipoCuenta, LocalDate> fechaDeposito;
    public static volatile SingularAttribute<TipoCuenta, Integer> saldo;
    public static volatile SingularAttribute<TipoCuenta, Long> id;
    public static volatile SingularAttribute<TipoCuenta, LocalDate> fechaRetiro;
    public static volatile SingularAttribute<TipoCuenta, String> nombre;
    public static volatile SingularAttribute<TipoCuenta, Long> version;
    public static volatile SingularAttribute<TipoCuenta, String> plazoTiempo;

}