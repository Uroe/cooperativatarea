package cr.ac.una.cooperativafe.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Navarro
 */
@Entity
@Table(name = "COOPFE_COOPERATIVAS", schema = "una")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cooperativa.findAll", query = "SELECT c FROM Cooperativa c"),
    @NamedQuery(name = "Cooperativa.findByCoopId", query = "SELECT c FROM Cooperativa c WHERE c.id = :id")})
public class Cooperativa implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "COOPFE_COOPERATIVAS_COOP_ID_GENERATOR", sequenceName = "una.COOPFE_COOPERATIVAS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COOPFE_COOPERATIVAS_COOP_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "COOP_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "COOP_NOMBRECOOP")
    private String nombreCoop;
    @Basic(optional = false)
    @Column(name = "COOP_NOMBREESC")
    private String nombreEsc;
    @Basic(optional = false)
    @Column(name = "COOP_NOMBREPROF")
    private String nombreProf;
    @Basic(optional = false)
    @Column(name = "COOP_ANOLECTIVO")
    private String anoLectivo;
    @Basic(optional = false)
    @Column(name = "COOP_ESTADO")
    private String estado;
    @Version
    @Basic(optional = false)
    @Column(name = "COOP_VERSION")
    private Long version;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "tcuenIdcoop")
    private TipoCuenta tipoCuenta;

    public Cooperativa() {
    }

    public Cooperativa(Long id) {
        this.id = id;
    }

    public Cooperativa(Long id, String nombreCoop, String nombreEsc, String nombreProf, String anoLectivo, String estado, Long version) {
        this.id = id;
        this.nombreCoop = nombreCoop;
        this.nombreEsc = nombreEsc;
        this.nombreProf = nombreProf;
        this.anoLectivo = anoLectivo;
        this.estado = estado;
        this.version = version;
    }
    
    public Cooperativa(CooperativaDto coopDto) {
        this.id = coopDto.getId();
        actualizarCooperativa(coopDto);
    }
    
    public void actualizarCooperativa(CooperativaDto coopDto){
        this.nombreCoop = coopDto.getNomCooperativa();
        this.nombreEsc = coopDto.getNomEscuela();
        this.nombreProf = coopDto.getNomProfesor();
        this.anoLectivo = coopDto.getAnoLectivo();
        this.estado = coopDto.getEstado();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreCoop() {
        return nombreCoop;
    }

    public void setNombreCoop(String nombreCoop) {
        this.nombreCoop = nombreCoop;
    }

    public String getNombreEsc() {
        return nombreEsc;
    }

    public void setNombreEsc(String nombreEsc) {
        this.nombreEsc = nombreEsc;
    }

    public String getNombreProf() {
        return nombreProf;
    }

    public void setNombreProf(String nombreProf) {
        this.nombreProf = nombreProf;
    }

     public String getAnoLectivo() {
        return anoLectivo;
    }

    public void setAnoLectivo(String anoLectivo) {
        this.anoLectivo = anoLectivo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public TipoCuenta getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(TipoCuenta tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cooperativa)) {
            return false;
        }
        Cooperativa other = (Cooperativa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.cooperativafe.model.Cooperativa[ coopId=" + id + " ]";
    }
    
}
