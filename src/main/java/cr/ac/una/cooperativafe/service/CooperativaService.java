/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.cooperativafe.service;

import cr.ac.una.cooperativafe.model.Cooperativa;
import cr.ac.una.cooperativafe.model.CooperativaDto;
import cr.ac.una.cooperativafe.util.EntityManagerHelper;
import cr.ac.una.cooperativafe.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

/**
 *
 * @author Navarro
 */
public class CooperativaService {
    
    EntityManager em = EntityManagerHelper.getInstance().getManager();
    private EntityTransaction et;
    
    public Respuesta getCooperativa(Long id) {
        try {
            Query qryCooperativa = em.createNamedQuery("Cooperativa.findByCoopId", Cooperativa.class);
            qryCooperativa.setParameter("id", id);

            return new Respuesta(true, "", "", "Cooperativa", new CooperativaDto((Cooperativa) qryCooperativa.getSingleResult()));

        } catch (NoResultException ex) {
            return new Respuesta(false, "No existe una cooperativa con la identificacion ingresada.", "getCooperativa NoResultException");
        } catch (NonUniqueResultException ex) {
            Logger.getLogger(CooperativaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar la cooperativa.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar la cooperativa.", "getCooperativa NonUniqueResultException");
        } catch (Exception ex) {
            Logger.getLogger(CooperativaService.class.getName()).log(Level.SEVERE, "Ocurrio un error obteniendo la cooperativa [" + id + "]", ex);
            return new Respuesta(false, "Ocurrio un error obteniendo la cooperativa.", "getCooperativa" + ex.getMessage());
        }
    }
    
    public Respuesta getCooperativas(){
        try{
            Query qryCooperativa = em.createNamedQuery("Cooperativa.findAll",Cooperativa.class);
            
            List<Cooperativa> coop = (List<Cooperativa>)qryCooperativa.getResultList();
            List<CooperativaDto> coopDto = new ArrayList<CooperativaDto>();
            for (Cooperativa cooperativa : coop) {
                coopDto.add( new CooperativaDto(cooperativa));
            }
            return new Respuesta(true, "", "", "Cooperativa", coopDto);
            
        }catch (NonUniqueResultException ex) {
            Logger.getLogger(CooperativaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar la cooperativa.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar la cooperativa.", "getCooperativas NonUniqueResultException");
        }
    }
  
    public Respuesta guardarCooperativa(CooperativaDto coopDto) {
        try {
            et = em.getTransaction();
            et.begin();
            Cooperativa coop;
            if (coopDto.getId() != null && coopDto.getId() > 0) {
                coop = em.find(Cooperativa.class, coopDto.getId());
                if (coop == null) {
                    et.rollback();
                    return new Respuesta(false, "No se encontró la cooperativa a modificar", "guardarCooperativa NoResultException");
                }
                coop.actualizarCooperativa(coopDto);
                coop = em.merge(coop);
            } else {
                coop = new Cooperativa(coopDto);
                em.persist(coop);
            }
            et.commit();
            return new Respuesta(true, "", "", "Cooperativa", new CooperativaDto(coop));
        } catch (Exception ex) {
            et.rollback();
            Logger.getLogger(CooperativaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardando la cooperativa.", ex);
            return new Respuesta(false, "Ocurrio un error al guardando la cooperativa.", "guardarCooperativa " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarCooperativa(Long id) {
        try {
            et = em.getTransaction();
            et.begin();
            Cooperativa coop;
            if (id != null && id > 0) {
                coop = em.find(Cooperativa.class, id);
                if (coop == null) {
                    et.rollback();
                    return new Respuesta(false, "No se encontró la cooperativa a eliminar", "eliminarCooperativa NoResultException");
                }
                em.remove(coop);
            } else {
                et.rollback();
                return new Respuesta(false, "Debe cargar la cooperativa a eliminar", "eliminarCooperativa NoResultException");
            }
            et.commit();
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            et.rollback();
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, "No se puede eliminar la cooperativa porque tiene relaciones con otros registros", "eliminarCooperativa" + ex.getMessage());
            }
            Logger.getLogger(CooperativaService.class.getName()).log(Level.SEVERE, "Ocurio un error al eliminar la cooperativa.", ex);
            return new Respuesta(false, "Ocuarrio un error al eliminar la cooperativa.", "eliminarCooperativa" + ex.getMessage());
        }
    }
}
